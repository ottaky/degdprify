function doIt() {

  let craptify = document.getElementsByClassName('captify-widget')
  if (craptify.length) {
    while (craptify.length) { craptify[0].remove() }
  }
  else {
    setTimeout(() => {

           if (quantcast()) { }
      else if (byId()) { }
      else {

        let allDivs = document.getElementsByTagName('div')

             if (spon(allDivs))    { }
        else if (qz(allDivs))      { }
        else if (med(allDivs))     { }
        else if (timeOut(allDivs)) { }
        else if (byClass(allDivs)) { }
        else {
          setTimeout(() => {
            if(rxn(allDivs)) { }
          }, 1000)
        }
      }
   }, 1000)
  }
}

function timeOut(allDivs) {
  for (let i = 0; i < allDivs.length; i++) {
    if (allDivs[i].classList.contains('cookie_banner')) {
      allDivs[i].remove()
      document.getElementsByTagName('body')[0].style.overflowY = 'scroll'
      document.getElementsByTagName('body')[0].style.overflowX = 'scroll'
      return true
    }
  }
  return false
}

function rxn(allDivs) {
  /* openculture advertising nag modal */
  for (let i = 0; i < allDivs.length; i++) {
    if (allDivs[i].classList.contains('rxn-modal-message')) {
      allDivs[i].parentElement.remove()
      return true
    }
  }
  return false
}

function quantcast() {
  if (document.getElementsByTagName('body')[0].classList.contains('qc-cmp-ui-showing')) {
    let crap = document.getElementsByClassName('qc-cmp-ui-container')
    while (crap.length) { crap[0].remove() }
    document.getElementsByTagName('body')[0].classList.remove('qc-cmp-ui-showing')
    return true
  }
  return false
}

function byId() {

  let badIds = [
    'privacy-consent',
    'gdpr',
    'cmp-container-id',
    '_evidon-barrier-wrapper'
  ]

  for (let i = 0; i < badIds.length; i++) {
    let crap = document.getElementById(badIds[i])
    if (crap) {
      crap.remove()
      return true
    }
  }
  return false
}

function spon(allDivs) {
  let spId = false;
  for (let i = 0; i < allDivs.length; i++) {
    if (allDivs[i].getAttribute('id')) {
      let matches = allDivs[i].getAttribute('id').match(/^sp_message_id([0-9a-zA-z]+)/)
      if (matches) {
        allDivs[i].remove()
        document.getElementsByTagName('body')[0].style.overflowY = 'scroll'
        document.getElementsByTagName('html')[0].style.overflowY = 'scroll'
        document.getElementsByClassName(`sp_veil${matches[1]}`)[0].remove()
        return true
      }
    }
  }

  // legacy version

  for (let i = 0; i < allDivs.length; i++) {
    for (let c = 0; c < allDivs[i].classList.length; c++) {
      let matches = allDivs[i].classList.item(c).match(/^sp_veil([0-9]+)$/)
      if (matches) {
        spId = matches[1]
        break
      }
    }
    if (spId) break
  }
  if (spId) {
    let crap = document.getElementsByClassName('sp_veil' + spId)
    while (crap.length) { crap[0].remove() }
    document.getElementById('sp_message_id' + spId).remove()
    document.getElementsByTagName('body')[0].style.overflowY = 'scroll'
    return true
  }
  return false
}

function qz(allDivs) {
  for (let i = 0; i < allDivs.length; i++) {
    for (let c = 0; c < allDivs[i].classList.length; c++) {
      if (allDivs[i].classList.item(c).match(/^ConsentForm__container/)) {
        allDivs[i].remove()
        return true
      }
    }
  }
  return false
}

function med(allDivs) {
  /* not actually gdpr, but equally annoying */
  for (let i = 0; i < allDivs.length; i++) {
    if (allDivs[i].classList.contains('overlay--lighter')) {
      allDivs[i].remove()
      document.getElementsByTagName('html')[0].classList.remove('u-overflowHidden')
      return true
    }
  }
  return false
}

function byClass(allDivs) {

  let badClasses = [
    'mol-ads-cmp',
    'site-message--first-pv-consent',
    'e1i2wxpm0',
    'sncmp-app_gdpr',
    'js-double-site-message',
    'butterBar',
    '_07fe7'
  ]

  for (let i = 0; i < allDivs.length; i++) {
    for (let c = 0; c < badClasses.length; c++) {
      if (allDivs[i].classList.contains(badClasses[c])) {
        allDivs[i].remove()
        return true
      }
    }
  }
  return false
}

if (document.readyState === 'complete') {
  doIt()
}
else {
  document.onreadystatechange = function () {
    if (document.readyState === "complete") {
      doIt()
    }
  }
}
